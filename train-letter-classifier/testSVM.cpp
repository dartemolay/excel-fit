#include "parameters.h"
#include "getFeatures.h"
#include "getPositions.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <map>

#include "liblinear/linear.h"

#include <opencv2/ml/ml.hpp>
#include <opencv2/core/core.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/features2d/features2d.hpp>

using namespace std;
using namespace cv;

#define HEIGHT 40
#define WIDTH  20
#define MAX_IMG_ON_ROW 40

/************************* PROCESS INPUT IMAGE *************************/

void processImage (Mat src, char znak, int count, model* model)
{
    int TP = 0;
    int FP = 0;

    /// For all letters in image
    for (int i=0; i < count; i++)
    {
        /// Get actual rectangle
        int inX = i % MAX_IMG_ON_ROW;
        int inY = i / MAX_IMG_ON_ROW;

        CvRect rect;
        rect.x = inX*WIDTH;
        rect.y = inY*HEIGHT;
        rect.width  = WIDTH;
        rect.height = HEIGHT;

        Mat roi(src, rect);

        /// Get features from rectangle
        vector<double> featureVec = getFeatures(roi);

        /// Prepare struct of features 
        int samples = featureVec.size();
        feature_node * features = new feature_node [samples+1];
        
        for (int i=0; i < featureVec.size(); i++){ 
            features[i].index = i;
            features[i].value = featureVec[i];
        }
        features[samples].index = -1;

        /// SVM - Predict 
        double result = predict(model, features);
//        cout << "result:  " << result << "      ";

        if (result == ((int)znak - (int)'0')) 
            TP++;
        else 
            FP++;

//        if (result == )

        /// SVM - Predict values
//      double value = 

        /// SVM - Predict with probability
//      double prob_estimates [3];
//      double probability = predict_probability(model, features, prob_estimates);
//      cout << "predict:  " << probability << endl;
//
//      cout << prob_estimates[0] << endl;
//      cout << prob_estimates[1] << endl;
//      cout << prob_estimates[2] << endl;

//        imshow("letter",roi);
//        waitKey(0);
	}

    cout << "TP: " << TP << endl;
    cout << "FP: " << FP << endl;
    cout << "Celkem: " << count << endl;
    cout << "Uspesnost: " << TP*100/count << " %" << endl;
}

/******************************* MAIN **********************************/

int main (int argc, char ** argv)
{
    /// Load input parameters 
    string inputDir = getParams(argc, argv);

    /// Get content of input directory
    vector<string> filesInDir = getFilesFromDir(inputDir);

    /// Create map and fill it with number of next image with negative samples
    map<char, int> countMap = getCountOfLetters(inputDir+"/count.dat");
 
    /// Load SVM classifier
    model* model = load_model("classifier");

    /// Print trained letters
    int poc = get_nr_class(model);
    int label [poc];
    get_labels(model, label);
    cout << "Trained letters:  ";
    for (int i=0; i<poc; i++)
        cout << label[i] << "  ";
    cout << endl;

    /// ================================
    /// Go throw all images in directory
    cout << "-------------------------" << endl;
    cout << "Process this letters:  " << endl;
    for (int i = 0; i < filesInDir.size(); i++)
    {
        string imgName = filesInDir[i];
        char znak = getCharacter(imgName);

        /// Load image
        Mat src;
        src = imread(imgName);
        if(!src.data)
            continue;
  
        /// Print actual letter
        cout << "-----> " << znak << " <-----" << endl;

        /// Process image
        processImage(src, znak, countMap[znak], model);
    }

    return 0;
}