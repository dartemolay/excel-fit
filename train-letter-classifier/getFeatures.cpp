#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace cv;

/******************** GLOBAL VARIABLES AND CONSTANTS *******************/

#define HEIGHT 40
#define WIDTH  20
#define MAX_IMG_ON_ROW 40

vector<int> brightnessVec;
vector<double> normalizeVec;
vector<int> gradientXVec;
vector<int> gradientYVec;

/************************** STRUCT DEFINITION **************************/

typedef struct {
    int pix;    // number of pixel
    double br;  // brightness
    int canX;   // canny in X
    int canY;   // canny in Y
} featureStruct;

/*********************** GET BRIGHTNESS OF PIXEL ***********************/

vector<int> getBrightness (Mat gray, int &min, int &max)
{
    vector<int> brightnessVec;
    brightnessVec.clear();

    /// Get brightness from gray image
    for (int h=0; h < gray.rows; h++)
        for (int w=0; w < gray.cols; w++){
            int val = (int)gray.at<uchar>(h,w);
            brightnessVec.push_back(val);
            if (val < min) min = val;
            if (val > max) max = val;
        }

    return brightnessVec;
}

/********************* NORMALIZE BRIGHTNESS VALUES *********************/

vector<double> getNormalizedBrightness(Mat norm, int min, int max)
{
    vector<double> normalizeVec;
    normalizeVec.clear();

    /// Normalize values in matrix
    for (int h=0; h < HEIGHT; h++)
        for (int w=0; w < WIDTH; w++){   
            int jas = brightnessVec[h*WIDTH + w];
            norm.at<uchar>(h,w) = round(255.0 / (max-min) * (jas-min));
            
            double val = 1.0 / (max-min) * (jas-min);         
            normalizeVec.push_back(val);
        }
        
    return normalizeVec;
}

/****************** GET GRADIENT X/Y OF SOBEL OPERATOR *****************/

int scale = 1;
int delta = 0;
int ksize = 3;
int ddepth = CV_16S;

vector<int> getSobelGradient (Mat gray, int x, int y)
{
    /// Process sobel operator and get gradient x
    Mat grad, abs_grad;

    GaussianBlur(gray, gray, Size(3,3), 0, 0, BORDER_DEFAULT);

    Sobel(gray, grad, ddepth, x, y, ksize, scale, delta, BORDER_DEFAULT);
    convertScaleAbs(grad, abs_grad);

    /// Prepare vector of gradients
    vector<int> gradientVec;
    gradientVec.clear();

    for (int i=0; i<abs_grad.rows; i++)
        for (int j=0; j<abs_grad.cols; j++){
            int val = (int)abs_grad.at<uchar>(i,j);
            gradientVec.push_back(val);
        }

    return gradientVec;
}

/***********************************************************************/
/***************************** GET FEATURES ****************************/

vector<double> getFeatures (Mat src)
{
    /// Convert to grayscale
    Mat gray;
    cvtColor(src, gray, CV_BGR2GRAY);

    /// Get value of brightness, min and max value
    int min = 255;
    int max = 0;
    brightnessVec = getBrightness(gray, min, max);

    /// Normalize brightness to interval <0,1>
    Mat norm(HEIGHT, WIDTH, CV_8UC1, 0.0);
    normalizeVec = getNormalizedBrightness(norm, min, max);

    /// Sobel detector - gradient X
    gradientXVec = getSobelGradient(gray, 1, 0);

    /// Sobel detector - gradient Y
    gradientYVec = getSobelGradient(gray, 0, 1);

    /// Prepare feature vector
    vector<double> featureVect;

    for (int i=0; i<normalizeVec.size(); i++) 
        featureVect.push_back(normalizeVec[i]);
    for (int i=0; i<gradientXVec.size(); i++) 
        featureVect.push_back((double)gradientXVec[i]);
    for (int i=0; i<gradientYVec.size(); i++) 
        featureVect.push_back((double)gradientYVec[i]);

    return featureVect;
}

//=======================================================================
//=======================================================================
//=======================================================================

int   canny_X    [WIDTH][HEIGHT];
int   canny_Y    [WIDTH][HEIGHT];

/********************** GET CANNY IN X COORDINATE **********************/

void getCannyX (Mat thresh)
{
    Mat imgX(HEIGHT, WIDTH, CV_8UC1, 0.0);
    int act, prev;
    
    /// Get canny detector in X coordinate
    for (int h=0; h < thresh.rows; h++)
        for (int w=1; w < thresh.cols; w++){
            prev = thresh.at<uchar>(h,w-1);
            act  = thresh.at<uchar>(h,w);
            if (act != prev) imgX.at<uchar>(h,w) = 255;
        }

    /// Fill matrix of integers
    for (int h=0; h < imgX.rows; h++)
        for (int w=0; w < imgX.cols; w++){
            int val = (int)imgX.at<uchar>(h,w);
            if (val == 0)
                canny_X[w][h] = 0; 
            else
                canny_X[w][h] = 1; 
        }         
}

/********************** GET CANNY IN Y COORDINATE **********************/

void getCannyY (Mat thresh)
{
    Mat imgY(HEIGHT, WIDTH, CV_8UC1, 0.0);
    int act, prev;
    
    /// Get canny detector in Y coordinate
    for (int h=1; h < thresh.rows; h++)
        for (int w=0; w < thresh.cols; w++){
            prev = thresh.at<uchar>(h-1,w);
            act  = thresh.at<uchar>(h,w);
            if (act != prev) imgY.at<uchar>(h,w) = 255;
        }

    /// Fill matrix of integers
    for (int h=0; h < imgY.rows; h++)
        for (int w=0; w < imgY.cols; w++){
            int val = (int)imgY.at<uchar>(h,w);
            if (val == 0)
                canny_Y[w][h] = 0; 
            else
                canny_Y[w][h] = 1; 
        } 
}
