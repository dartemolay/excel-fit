#ifndef _getPositions_h
#define _getPositions_h

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>    
#include <vector>
#include <string>
#include <iostream>
#include <dirent.h>
#include <map>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/features2d/features2d.hpp>

using namespace std;
using namespace cv;

Mat drawBoundingBox(Mat src, CvRect boundRect);

// Find rectangles via contours
vector<CvRect> findContourInImage (Mat src);
Mat contrast(Mat src);

// Find rectangles via MSER
vector<CvRect> findMserInImage (Mat src);

#endif