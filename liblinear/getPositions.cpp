#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>    
#include <vector>
#include <string>
#include <iostream>
#include <dirent.h>
#include <map>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/features2d/features2d.hpp>

using namespace std;
using namespace cv;



/************************* Draw bounding box **************************/
Mat drawBoundingBox(Mat src, CvRect boundRect)
{
    Mat boundImg;
    src.copyTo(boundImg);
    rectangle(boundImg, Point(boundRect.x,boundRect.y), Point(boundRect.x+boundRect.width,boundRect.y+boundRect.height), Scalar(0,255,0), 1);
    return boundImg;
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*               CONTOURS  CONTOURS  CONTOURS  CONTOURS               */
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/


/***************** CHECK SIZE OF RECTANGLE ************************/

bool checkSizeOfRectangle (CvRect rect, Mat src)
{
    //if (rect.width >= rect.height) return false;
    if (rect.height < 12)          return false;
    if (rect.height < 6)           return false;
    if (src.rows/2 >= rect.height) return false;
    if (src.cols/2 <= rect.width)  return false;

    double pomer = (double)rect.height / (double)rect.width;
    //cout << pomer << endl;

    if (pomer > 3.0) return false;
    //if (pomer < 0.9) return false;
    
    return true;
}

/********************** PROCESS RECTANGLES **************************/

vector<CvRect> processRectangles (Mat src, vector<Rect> boundRect)
{
    //Mat drawing1, drawing2, drawing3;
    //src.copyTo(drawing1);
    //src.copyTo(drawing2);
    
    vector<CvRect> rectVect1;
    vector<CvRect> rectVect2;

    // check size of rectangles, draw rectangles
    for (int i=0; i<boundRect.size(); i++){
        //rectangle(drawing1, Point(boundRect[i].x,boundRect[i].y), Point(boundRect[i].x+boundRect[i].width,boundRect[i].y+boundRect[i].height), Scalar(0,255,0), 1, 4, 0);
        if (!checkSizeOfRectangle(boundRect[i], src)) continue;
        //rectangle(drawing2, Point(boundRect[i].x,boundRect[i].y), Point(boundRect[i].x+boundRect[i].width,boundRect[i].y+boundRect[i].height), Scalar(0,255,0), 1, 4, 0);    
        rectVect1.push_back(boundRect[i]);
    }
    
    // get minimal width in vector
    int min_width = 0;
    for (int i=0; i<rectVect1.size(); i++){
        if (i==0) 
            min_width = rectVect1[i].width;
        else 
            if (min_width > rectVect1[i].width) 
                min_width = rectVect1[i].width;
    }

    // division multiletters rectangles
    for (int i=0; i<rectVect1.size(); i++){
        double del = round((double)rectVect1[i].width / (double)min_width);
        if (del == 1){
            rectVect2.push_back(rectVect1[i]); 
        }
        else {
            int step = rectVect1[i].width / del;
            int x = rectVect1[i].x;

            for (int j=0; j < del; j++){
                CvRect r;
                r.x = x;
                r.y = rectVect1[i].y;
                r.width = min_width;
                r.height = rectVect1[i].height;
                x += step;
                rectVect2.push_back(r);
            }
        }
    }

    //for (int i=0; i<rectVect2.size(); i++){
    //    rectangle(drawing1, Point(rectVect2[i].x,rectVect2[i].y), Point(rectVect2[i].x+rectVect2[i].width,rectVect2[i].y+rectVect2[i].height), Scalar(0,255,0), 1, 4, 0);    
    //}

    //imshow("drawing2", drawing2);
    //imshow("drawing1", drawing1);
    //waitKey(0);

    return rectVect2;
}

/************************ FIND CONTOUR **************************/

vector<CvRect> findContourInImage (Mat src)
{
    Mat gray;
    cvtColor(src, gray, CV_BGR2GRAY);

    Mat threshold_output;
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;

    // Detect edges using Threshold
    threshold( gray, threshold_output, 100, 255, THRESH_BINARY);
    // Find contours
    findContours( threshold_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
  
    // Approximate contours to polygons + get bounding rects and circles
    vector<vector<Point> > contours_poly( contours.size() );
    vector<Rect> boundRect( contours.size() );

    for( int i = 0; i < contours.size(); i++ ){ 
        boundRect[i] = boundingRect( Mat(contours[i]) );
    }

    vector<CvRect> rectVect = processRectangles(src, boundRect);
    return rectVect;
}

/************************* ZVYSENI KONTRASTU **************************/

Mat contrast(Mat src)
{
    Mat new_image = Mat::zeros( src.size(), src.type() );

    double alpha = 2.0; 
    int    beta  = 20; 

    // Do the operation new_image(i,j) = alpha*image(i,j) + beta
    for( int y = 0; y < src.rows; y++ )
        for( int x = 0; x < src.cols; x++ )
            for( int c = 0; c < 3; c++ )
                new_image.at<Vec3b>(y,x)[c] = saturate_cast<uchar>( 2.0*( src.at<Vec3b>(y,x)[c] ) + 20 );
 
    //vector<CvRect> rectVect = findCountourInImage(new_image);
    return new_image;
}


/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*                  MSER  MSER  MSER  MSER  MSER  MSER                */
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/


/************************** GLOBAL VARIABLES **************************/

int maxY = 0;
int minY = 0;
int maxYvalue = 0;

/***************************** Check size *****************************/

bool checkSize (CvRect boundRect)
{
    if (boundRect.height <= boundRect.width) return false;

    double pomer = (double)boundRect.height / (double)boundRect.width;
    //cout << pomer << endl;
    if ((pomer < 1.8) || (pomer > 2.2)) return false;

    return true;
}

/******************* Bounding box around MSER area ********************/

CvRect getPositionOfMSER(vector<Point> mser, Mat image)
{
    // create bounding box around MSER area
    vector<Point> contour;
    approxPolyDP(Mat(mser), contour, 3, true);
    CvRect boundRect = boundingRect(Mat(contour));
    return boundRect;
}

bool showMSERimg(Mat image, vector<Point> mser, CvRect boundRect)
{
    if (!checkSize(boundRect)) return false;

    Mat mserImg = Mat::zeros(image.rows,image.cols,CV_8UC1);

    for (int j = 0; j < (int)mser.size(); j++)
    {
        cv::Point point = mser[j];
        mserImg.at<char>(point.y,point.x) = 255;
    }

    mserImg = Mat(mserImg,boundRect);

    return true;
}

/************* Get width and height of character position *************/

void getHeightPositionOfCharacter(vector<CvRect> vectRect, int rows)
{
    minY = 0;
    maxY = 0;
    maxYvalue = 0;

    int array [rows];
    for (int i=0; i<rows; i++) array[i]=0;

    for (int i = 0; i < vectRect.size(); i++) 
        for (int j=vectRect[i].y; j<(vectRect[i].y+vectRect[i].height); j++)                    
            array[j]+=1;
    
    for (int i=0; i<rows; i++){
        if (array[i] > 0) {
            if (minY == 0) minY = i;
            maxY = i;
        }
        if (i == 0) 
            maxYvalue = array[i];
        else
            if (maxYvalue < array[i])
                maxYvalue = array[i];
    }

//    cout << "... " << minY << " " << maxY << " " << maxYvalue << " - " << maxYvalue/3 << endl;
}

vector<CvRect> getWidthPositionOfCharacters(vector<CvRect> vectRect, int cols)
{
    vector<CvRect> vect;
    
    int array [cols];
    for (int i=0; i<cols; i++) array[i]=0;
 
    for (int i = 0; i < vectRect.size(); i++) {
        for (int j=vectRect[i].x; j<(vectRect[i].x+vectRect[i].width); j++)                    
            array[j]+=1;
    }

    CvRect rect;
    rect.y = minY;
    rect.height = maxY - minY;

    for (int i=1; i<cols; i++){
        if ((array[i-1] == 0) && (array[i] != 0))
            rect.x = i;
        if ((array[i-1] != 0) && (array[i] == 0)){
            rect.width = i-1 - rect.x;
            vect.push_back(rect);
        }
    }
    return vect;
}

/*********************** Find letter positions ************************/

vector<CvRect> findMserInImage (Mat src)
{
    vector<CvRect> pos;

    // convert to grayscale
    Mat gray;
    cvtColor(src, gray, CV_BGR2GRAY);

    // prepare MSER
    Mat MSERImage;
    gray.copyTo(MSERImage);
    vector<vector<Point> > mserVec;

    // detect MSER
    MSER mser = cv::MSER();
    mser(gray, mserVec);

    Mat out;
    src.copyTo(out);
    vector<CvRect> vectRect;

    // get MSERs which can be letters
    for (int i = 0; i < mserVec.size(); i++)
    {
        // get position of MSER
        CvRect boundRect = getPositionOfMSER(mserVec[i],MSERImage);
        if (!showMSERimg(MSERImage, mserVec[i], boundRect)) continue;
        out = drawBoundingBox(out, boundRect);
        if (!checkSizeOfRectangle(boundRect, src)) continue;
        vectRect.push_back(boundRect);
    }

    Mat out1;
    out.copyTo (out1);
    //imshow("out1", out1);

    // get positions of characters
    getHeightPositionOfCharacter(vectRect, src.rows);

    vector<CvRect> vectCharacters;
    vectCharacters = getWidthPositionOfCharacters(vectRect, src.cols);

    src.copyTo(out);
    for (int i = 0; i < vectCharacters.size(); i++)
        out = drawBoundingBox(out, vectCharacters[i]);
    //imshow("licence plate", out);
    //waitKey(0);

    return vectCharacters;
}