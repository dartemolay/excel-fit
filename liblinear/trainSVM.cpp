#include "parameters.h"
#include "getFeatures.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <map>

#include "liblinear/linear.h"

#include <opencv2/ml/ml.hpp>
#include <opencv2/core/core.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/features2d/features2d.hpp>

using namespace std;
using namespace cv;
 
/************************* CONSTANT DEFINITION *************************/

#define HEIGHT 40
#define WIDTH  20
#define MAX_IMG_ON_ROW 40

/************************** GLOBAL VARIABLES **************************/

vector<vector<double> > featVecVec;
vector<double> labelsVec;

/********************** CONVERT NUMBER TO STRING ***********************/

template <typename T>
string NumberToString ( T Number )
{
    ostringstream ss;
    ss << Number;
    return ss.str();
}

/*************************** TRAIN SVM STRUCTS *************************/

void trainSVM ()
{

//double * aaa = new double [1000];
//for (int i=0; i < 1000; i++)
//    aaa[i] = i;
//
//cout << sizeof(double)*1000 << endl;

    struct parameter param;
    struct problem prob;

    /// Fill structure of SVM parameters
    param.solver_type = MCSVM_CS; 
    param.C = 1;
    param.eps = 0.00001;// numeric_limits<double>::infinity();     // 0.005
    param.nr_weight = 0;
    param.weight_label = NULL;
    param.weight = NULL;
    param.p = 0.1;

    /// Get total count of all letters
    int samplesCount = featVecVec.size();
    int featureCount = featVecVec[0].size();


    /// Prepare array of labels
    double * labels = new double [samplesCount];

   for (int i=0; i < samplesCount; i++) 
        labels[i] = labelsVec[i];

    /// Prepare training data
    feature_node ** trainingData = new feature_node * [samplesCount]; 
    for (int i=0; i < samplesCount; i++) 
        trainingData[i] = new feature_node[featureCount+1];

    for (int i=0; i < samplesCount; i++){
        for (int j=0; j < featureCount; j++){
            trainingData[i][j].index = j+1;
            trainingData[i][j].value = featVecVec[i][j];
        }
        trainingData[i][featureCount].index = -1;
    }

    /// Fill structure of problem
    prob.l = samplesCount;    // count of samples
    prob.n = featureCount;    // count of features
    prob.bias = 1;
    prob.y = labels;
    prob.x = trainingData;

cout << endl << "---> start train" << endl << endl;

    /// Train SVM classifier
    model* modeL = train(&prob,&param);

cout << endl << "---> stop train" << endl << endl;

    /// Save SVM classifier
    save_model("classifier", modeL);
}

/************************* PROCESS INPUT IMAGE *************************/

void processImage (Mat src, char znak, int count)
{
    /// For all letters in image
    for (int i=0; i < count; i++)
    {
        /// Get actual rectangle
        int inX = i % MAX_IMG_ON_ROW;
        int inY = i / MAX_IMG_ON_ROW;

        CvRect rect;
        rect.x = inX*WIDTH;
        rect.y = inY*HEIGHT;
        rect.width  = WIDTH;
        rect.height = HEIGHT;
        Mat roi(src, rect);

        /// Get features from rectangle
        vector<double> featureVec = getFeatures(roi);

        /// Save vector of letters feature 
        featVecVec.push_back(featureVec);

        /// Fill letters vector
        double x = (double)znak - (double)'0';
        double zn = (x >= 17) ?  (x-7) : (x);
        labelsVec.push_back(zn);
    }
}

/******************************* MAIN **********************************/

int main (int argc, char ** argv)
{
    /// Load input parameters 
    string inputDir = getParams(argc, argv);

    /// Get content of input directory
    vector<string> filesInDir = getFilesFromDir(inputDir);

    /// Get count of letters
    map<char, int> countMap = getCountOfLetters(inputDir+"/count.dat");

    /// ================================
    /// Go throw all images in directory
    cout << "Process this letters:  ";
    for (int i = 0; i < filesInDir.size(); i++)
    {
        string imgName = filesInDir[i];
        char znak = getCharacter(imgName);

        /// Load image
        Mat src;
        src = imread(imgName);
        if(!src.data)
            continue;
        
        /// Print character
        cout << znak << "  ";

        /// Process image
        processImage(src, znak, countMap[znak]);
    }

    cout << endl;
    cout << "Features are prepared." << endl;
    cout << "Training..." << endl;
      
    /// ================================
    /// Train SVM classifier
    trainSVM();

    cout << endl << "Training of SVM classifier ends." << endl;

    return 0;
}
