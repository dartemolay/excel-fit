#include "parameters.h"
#include "getFeatures.h"
#include "getPositions.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include "liblinear/linear.h"

#include <opencv2/ml/ml.hpp>
#include <opencv2/core/core.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/features2d/features2d.hpp>

using namespace std;
using namespace cv;

/************************* PROCESS INPUT IMAGE *************************/

void processImage (Mat src, model* model)
{
    /// Convert source image to gray scale
    Mat gray;
    cvtColor(src, gray, CV_BGR2GRAY);

	/// Find position of letters in licence plates
    vector<CvRect> positionsMSER = findMserInImage(src);
    vector<CvRect> positionsContour = findContourInImage(src);

    /// Get final positions of letters
    vector<CvRect> characterPositions;
    characterPositions = positionsContour;

	/// Show positions of letters
    Mat box;
    src.copyTo(box);
    for (int i=0; i<characterPositions.size(); i++){
        CvRect r = characterPositions[i];
        box = drawBoundingBox(box, r);
    }
    imshow("box",box);
    
    /// Go throw all letters in image
    for (int i=0; i<characterPositions.size(); i++){
        int per15 = 15 * characterPositions[i].width / 100;
        int per40 = 40 * characterPositions[i].width / 100;

        CvRect r = characterPositions[i];
        r.x = r.x - per15;
        r.y = r.y - per40;
        r.width  = r.width  + 2*per15;
        r.height = r.height + 2*per40;

        /// Get rectangle of interest = letters
        Mat roi;
        Mat roi1 (src, r);
        roi1.copyTo(roi);

        imshow("ROI",roi);
        //resize(roi, roi, cv::Size(20, 40));
        //imshow("RESIZE",roi);

		/// Get features from rectangle
	    vector<double> featureVec = getFeatures(roi);

	    /// Prepare struct of features 
	    int samples = featureVec.size();
	    feature_node * features = new feature_node [samples+1];
	    
	    for (int i=0; i < featureVec.size(); i++){ 
	     	features[i].index = i;
	        features[i].value = featureVec[i];
	    }
	    features[samples].index = -1;

		cout << endl;

	    /// SVM - Predict 
		double result = predict(model, features);
		cout << "result:  " << result << "      ";

		/// SVM - Predict values
		double value = 

		/// SVM - Predict with probability
		double prob_estimates [3];
		double probability = predict_probability(model, features, prob_estimates);
		cout << "predict:  " << probability << endl;

		cout << prob_estimates[0] << endl;
		cout << prob_estimates[1] << endl;
		cout << prob_estimates[2] << endl;

        waitKey(0);
	}
}

/******************************* MAIN **********************************/

int main (int argc, char ** argv)
{
    /// Load input parameters 
    string inputDir = getParams(argc, argv);

    /// Get content of input directory
    vector<string> filesInDir = getFilesFromDir(inputDir);

    /// Load SVM classifier
    model* model = load_model("classifier");

    /// Print trained letters
    int poc = get_nr_class(model);
    int label [poc];
    get_labels(model, label);
    cout << "Trained letters:  ";
    for (int i=0; i<poc; i++)
    	cout << label[i] << "  ";
    cout << endl;

    /// ================================
    /// Go throw all images in directory
    cout << "Process this letters:  ";
    for (int i = 0; i < filesInDir.size(); i++)
    {
        string imgName = filesInDir[i];
        cout << imgName << endl;

        /// Load image
        Mat src;
        src = imread(imgName);
        if(!src.data)
            continue;
  
        /// Resize input image
        resize(src, src, cv::Size(src.cols*2, src.rows*2));

        /// Process image
        processImage(src, model);
    }

    return 0;
}