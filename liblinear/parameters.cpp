#include <iostream>
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <dirent.h>
#include <vector>
#include <map>

using namespace std;

/************************ PROCESS PARAMETERS ***************************/

string getParams(int argc, char *argv[])
{   
    // prepare struct
    string inputDir = "";
 
    // get values from input
    int c;
    extern char * optarg;
    while ((c = getopt(argc, argv, "d:h")) != -1){
        switch(c) {
            case 'd':
                inputDir = optarg;
                break;        
            case 'h':
                cout << "./main -d <directory with letters>" << endl;
                exit(0);
            default:
                cout << "./main -d <directory with letters>" << endl;
                exit(1);
        }       
    }

    if (inputDir.empty()) {
        cout << "./main -d <directory with letters>" << endl;
        exit(1);
    }

    return inputDir;
}

/********************** GET CONTENT OF DIRECTORY ***********************/

vector<string> getFilesFromDir (string inputDir)
{
    vector<string> vect;

    DIR *dir;
    class dirent *ent;
    
    dir = opendir(inputDir.c_str());
    while ((ent = readdir(dir)) != NULL) {
        string file_name = ent->d_name;
        
        if (file_name[0] == '.')
            continue;
        
        string file_path = inputDir + "/" + file_name;
        vect.push_back(file_path);
    }

    closedir(dir);
    return vect;
}

/************************ LOAD COUNT OF LETTERS ************************/

map<char,int> getCountOfLetters (string path)
{
    map<char,int> mapa;    

    ifstream numList(path.c_str());
    string line;

    while(getline(numList, line))
    {
        char znak; 
        int num;

        istringstream iss(line);
        iss >> znak >> num;      

        mapa[znak] = num;
    }

    return mapa;
}

/**************************** GET CHARACTER ****************************/

char getCharacter(string imgName)
{
    int find = imgName.find("img_");
    string znak = imgName.substr(find+4,1);

    return znak[0];
}

