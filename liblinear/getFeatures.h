#ifndef _getFeatures_h
#define _getFeatures_h

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace cv;

/**************************************************/

typedef struct {
    int pix;    // number of pixel
    double br;  // brightness
    int canX;   // canny in X
    int canY;   // canny in Y
} featureStruct;

/**************************************************/

vector<double> getFeatures (Mat src);

/**************************************************/

#endif