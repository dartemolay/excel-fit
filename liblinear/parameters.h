#ifndef _parameters_h
#define _parameters_h

#include <iostream>
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <dirent.h>
#include <vector>
#include <map>

using namespace std;

string getParams(int argc, char *argv[]);
vector<string> getFilesFromDir (string inputDir);
map<char,int> getCountOfLetters (string path);
char getCharacter(string imgName);

#endif